#include <iostream>
#include <cmath>

using namespace std;

/***********************************************************************************************************************
 IF GOING WITH CUSTOM PARAMETERS, i.e. (a,b) != (2,4) in the (a,b) tree:
 * define MAX as b-1
 * define MIN as a-1
***********************************************************************************************************************/

#define MAX 3 // MAX = 3 for 2-4 tree, = 2 for 2-3 tree
#define MIN 1

struct btreeNode
{
	int val[MAX + 1];   // since we start storing values from count 1, to make it easier
	int count;
	bool touched;   // to keep a track of touched nodes
	bool modified;  // to keep a track of modified nodes
	btreeNode *link[MAX + 1];   // to hold children
};

btreeNode *root;
double touchCount = 0;
double modCount = 0;
long no_ops = 0;
/***********************************************************************************************************************
************************************************** INSERT ROUTINE ******************************************************
***********************************************************************************************************************/

/* creating a new node */
btreeNode* createNode (int val, btreeNode *child)
{
	btreeNode *newNode = new btreeNode;
	newNode->val[1] = val;
	newNode->count = 1;
	newNode->link[0] = root;
	newNode->link[1] = child;
	newNode->modified = true;
	newNode->touched = false;
	return newNode;
}

/* Places the key in a node according to where it should be, used in split function */
void addValToNode (int val, int pos, btreeNode *node, btreeNode *child)
{
	int j = node->count;
	node->touched = true;
	node->modified = true;
	
	while (j > pos)
	{
		node->val[j + 1] = node->val[j];
		node->link[j + 1] = node->link[j];
		if (node->link[j+1])
			node->link[j + 1]->touched = node->link[j + 1]->modified = true;
		j--;
	}
	node->val[j + 1] = val;
	node->link[j + 1] = child;
	if (child)
		child->touched = true;
	if (node->link[j+1])
		node->link[j+1]->modified = node->link[j+1]->touched = true;
	node->count++;
}

/* split the node, moving median up and making sure the a,b property is maintained */
void splitNode (int val, int *pval, int pos, btreeNode *node, btreeNode *child, btreeNode **newNode)
{
	int median, j;
	if (pos > MIN)
		median = MIN + 1;
	else
		median = MIN;
	
	*newNode = new btreeNode;
	j = median + 1;
	while (j <= MAX)   // the keys being moved from a node
	{
		(*newNode)->val[j - median] = node->val[j];
		(*newNode)->modified = (*newNode)->touched = true;
		
		(*newNode)->link[j - median] = node->link[j];
		if ((*newNode)->link[j-median])
			(*newNode)->link[j - median]->touched = (*newNode)->link[j - median]->modified = true;
		
		j++;
	}
	
	node->touched = true;
	node->modified = true;
	node->count = median;
	(*newNode)->count = MAX - median;
	
	if (pos <= MIN)
		addValToNode(val, pos, node, child);
	else
		addValToNode(val, pos - median, *newNode, child);
	
	*pval = node->val[node->count];
	
	(*newNode)->link[0] = node->link[node->count];
	if ((*newNode)->link[0])
		(*newNode)->link[0]->modified = (*newNode)->link[0]->touched = true;
	node->count--;
}

/* sets the value val in the node. determines if the node needs to be created or not, after all necessary operations */
int setValueInNode (int val, int *pval, btreeNode *node, btreeNode **child)
{
	int pos;
	if (!node)  // node does not exist as of yet
	{
		*pval = val;
		*child = NULL;
		return 1;
	}
	
	node->touched = true;
	if (val < node->val[1])
		pos = 0;
	else
	{
		for (pos = node->count; (val < node->val[pos] && pos > 1); pos--);  // get correct position where it should be placed
		
		
		if (val == node->val[pos])  // duplicate value, ignored. won't have further operations.
			{
				no_ops--;
				return 0;
			}
	}
	
	// we have position to insert the value as of now.
	// now we insert
	
	if (setValueInNode(val, pval, node->link[pos], child))
	{
		if (node->count < MAX)
			addValToNode(*pval, pos, node, *child);
		
		else // more than b children
		{
			splitNode(*pval, pval, pos, node, *child, child);
			return 1;
		}
	}
	return 0;
}

/* insert val in B-Tree */
void insert (int val)
{
	int flag, i;
	btreeNode *child;
	
	flag = setValueInNode(val, &i, root, &child);
	if (flag)   // only if a new node is created, and not if it's a duplicate
		root = createNode(i, child);
}


/***********************************************************************************************************************
************************************************** DELETE ROUTINE ******************************************************
***********************************************************************************************************************/

/* copy successor for the value to be deleted */
void copySuccessor(btreeNode *myNode, int pos)
{
	btreeNode *dummy;
	dummy = myNode->link[pos];
	dummy->modified = true;
	for (; dummy->link[0] != NULL;)
	{
		dummy->touched = true;
		dummy->link[0]->touched = true;
		dummy = dummy->link[0];
		dummy->modified = true;
	}
	myNode->val[pos] = dummy->val[1];
	myNode->touched = dummy->touched = true;
	myNode->modified = true;
	
}

/* removes the value from the given node and rearrange values */
void removeVal(btreeNode *myNode, int pos)
{
	int i = pos + 1;
	while (i <= myNode->count)
	{
		myNode->val[i - 1] = myNode->val[i];
		myNode->link[i - 1] = myNode->link[i];
		myNode->link[i-1]->touched = myNode->link[i]->touched = true;
		myNode->link[i-1]->modified = true;
		i++;
	}
	myNode->count--;
	myNode->modified = true;
	myNode->touched = true;
}

/* shifts value from parent to right child */
void doRightShift(btreeNode *myNode, int pos)
{
	btreeNode *x = myNode->link[pos];
	x->touched = myNode->link[pos]->touched = true;
	x->modified = true;
	int j = x->count;
	
	while (j > 0)
	{
		x->val[j + 1] = x->val[j];
		x->link[j + 1] = x->link[j];
		x->link[j+1]->touched = x->link[j]->touched = true;
		x->link[j+1]->modified = true;
	}
	
	x->val[1] = myNode->val[pos];
	x->link[1] = x->link[0];
	x->link[1]->touched = x->link[0]->touched = true;
	x->link[1]->modified = true;
	x->count++;
	
	x = myNode->link[pos - 1];
	x->modified = true;
	x->touched = myNode->link[pos-1]->touched = true;
	
	myNode->val[pos] = x->val[x->count];
	myNode->touched = true;
	myNode->link[pos] = x->link[x->count];
	myNode->link[pos]->modified = true;
	myNode->link[pos]->touched = x->link[x->count]->touched = true;
	x->count--;
}

/* shifts value from parent to left child */
void doLeftShift(btreeNode *myNode, int pos)
{
	int j = 1;
	btreeNode *x = myNode->link[pos - 1];
	x->modified = true;
	x->touched = myNode->link[pos-1]->touched = true;
	
	x->count++;
	x->val[x->count] = myNode->val[pos];
	myNode->touched = true;
	x->link[x->count] = myNode->link[pos]->link[0];
	x->link[x->count]->touched = myNode->link[pos]->touched = myNode->link[pos]->link[0]->touched = true;
	x->link[x->count]->modified = true;
	
	x = myNode->link[pos];
	x->touched = myNode->link[pos]->touched = true;
	x->modified = true;
	
	myNode->val[pos] = x->val[1];
	myNode->touched = x->touched = true;
	myNode->modified = true;
	
	x->link[0] = x->link[1];
	x->link[0]->touched = x->link[1]->touched = true;
	x->link[0]->modified = true;
	
	x->count--;
	x->touched = x->modified = true;
	
	while (j <= x->count)
	{
		x->val[j] = x->val[j + 1];
		x->link[j] = x->link[j + 1];
		x->link[j]->touched = x->link[j+1]->touched = true;
		x->link[j]->modified = true;
		j++;
	}
	return;
}

/* merge nodes */
void mergeNodes(btreeNode *myNode, int pos)
{
	int j = 1;
	btreeNode *x1 = myNode->link[pos], *x2 = myNode->link[pos - 1];
	x1->touched = myNode->link[pos]->touched = true;
	x1->modified = true;
	
	x2->touched = myNode->link[pos-1]->touched = true;
	x2->modified = true;
	
	x2->count++;
	x2->val[x2->count] = myNode->val[pos];
	myNode->touched = true;
	
	x2->link[x2->count] = myNode->link[0];
	x2->link[x2->count]->touched = myNode->link[0]->touched = true;
	x2->link[x2->count]->modified = true;
	
	while (x1 && j <= x1->count)
	{
		x2->count++;
		x2->val[x2->count] = x1->val[j];
		x2->link[x2->count] = x1->link[j];
		x2->link[x2->count]->touched = x1->link[j]->touched = true;
		x2->link[x2->count]->modified = true;
		j++;
	}
	
	j = pos;
	while (j < myNode->count)
	{
		myNode->val[j] = myNode->val[j + 1];
		myNode->modified = true;
		myNode->link[j] = myNode->link[j + 1];
		myNode->link[j]->touched = myNode->link[j+1]->touched = true;
		myNode->link[j]->modified = true;
		j++;
	}
	myNode->count--;
	free(x1);
}

/* adjusts the given node */
void adjustNode(btreeNode *myNode, int pos)
{
	if (!pos)
	{
		if (myNode->link[1] && myNode->link[1]->count > MIN)
		{
			doLeftShift(myNode, 1);
			myNode->link[1]->touched = myNode->touched = true;
		}
		else
			mergeNodes(myNode, 1);
	}
	else
	{
		myNode->touched = true;
		if (myNode->count != pos)
		{
			if (myNode->link[pos - 1]->count > MIN)
			{
				doRightShift(myNode, pos);
				myNode->link[pos-1]->touched = true;
			}
			else
			{
				if (myNode->link[pos + 1]->count > MIN)
				{
					doLeftShift(myNode, pos + 1);
					myNode->link[pos+1]->touched = true;
				}
				else
					mergeNodes(myNode, pos);
			}
		}
		else
		{
			myNode->touched = true;
			if (myNode->link[pos - 1]->count > MIN)
			{
				doRightShift(myNode, pos);
				myNode->link[pos-1]->touched = true;
			}
			else
				mergeNodes(myNode, pos);
		}
	}
}

/* delete val from the node */
int delValFromNode(int val, btreeNode *myNode)
{
	int pos, flag = 0;
	if (myNode)
	{
		myNode->touched = true;
		if (val < myNode->val[1])
		{
			pos = 0;
			flag = 0;
		}
		else
		{
			for (pos = myNode->count; (val < myNode->val[pos] && pos > 1); pos--);
			
			if (val == myNode->val[pos])
				flag = 1;
			else
				flag = 0;
		}
		
		if (flag)
		{
			if (myNode->link[pos - 1])
			{
				myNode->link[pos-1]->touched = true;
				copySuccessor(myNode, pos);
				myNode->link[pos]->touched = true;
				flag = delValFromNode(myNode->val[pos], myNode->link[pos]);
				if (flag == 0)
					return flag;
			}
			else
				removeVal(myNode, pos);
		}
		else
			flag = delValFromNode(val, myNode->link[pos]);
		
		if (myNode->link[pos])
		{
			myNode->link[pos]->touched = true;
			if (myNode->link[pos]->count < MIN)
			{
				myNode->link[pos]->touched = true;
				adjustNode(myNode, pos);
			}
		}
	}
	return flag;
}

/* delete val from B-tree */
int deletion (int val, btreeNode *myNode)
{
	btreeNode *tmp;
	if (!delValFromNode(val, myNode))   // value not present
		return 0;
	else
	{
		if (myNode->count == 0)
		{
			tmp = myNode;
			tmp->touched = myNode->touched = true;
			tmp->modified = true;
			myNode = myNode->link[0];
			myNode->modified = myNode->touched = true;
			myNode->link[0]->touched = true;
			free (tmp);
		}
	}
	root = myNode;
	root->touched = root->modified = true;
	return 1;
}

/***********************************************************************************************************************
************************************************** DEBUG FUNCTIONS *****************************************************
***********************************************************************************************************************/


/* B-Tree Traversal. Auxiliary function for debugging */
void traverse (btreeNode *myNode)
{
	int i;
	if (myNode)
	{
		for (i = 0; i < myNode->count; i++)
		{
			traverse (myNode->link[i]);
			cout<< myNode->val[i + 1]<<" ";
		}
		traverse (myNode->link[i]);
	}
}

/***********************************************************************************************************************
************************************************** OTHER FUNCTIONS *****************************************************
***********************************************************************************************************************/
void delExtra (btreeNode *myNode)
{
	for(int i=myNode->count; i<=MAX; i++)
		if (myNode->val[i] != 0)
			myNode->val[i] = 0;
}

void counts(btreeNode *myNode)
{
	int i;
	if (myNode)
	{
		delExtra(myNode);
		for (i = 0; i < myNode->count; i++)
		{
			counts (myNode->link[i]);
			if (myNode->touched)
			{
				myNode->touched = false;
				touchCount++;
			}
			if (myNode->modified)
			{
				myNode->modified = false;
				modCount++;
			}
		}
		counts(myNode->link[i]);
	}
}


/***********************************************************************************************************************
************************************************** MAIN PROGRAM ******************************************************
***********************************************************************************************************************/


int main()
{
	char op_type;
	int value, set;
	int i = 0;
	long N = 0;
	while (scanf("%c %d\n", &op_type, &value) > 0) // read until EOF
	{
		if (op_type == '#')
		{
			if (root)
			{
				root = NULL;
				free(root);
				
				cout<<"set: "<<set<<" ("<<i<<")"<<endl;
				cout<<"ops: "<<no_ops<<endl;
				cout<<"Mod: "<<modCount<<" "<<modCount/no_ops<<endl;
				cout<<"Tch: "<<touchCount<<" "<<touchCount/no_ops<<endl;
				cout<<"Max: "<< floor(log((no_ops/2+1)/2)/log(2));
				cout<<"\n\n";
				modCount = 0;
				touchCount = 0;
				no_ops = 0;
				N = 0;
			}
			set = value;
			i++;
		}
		if (op_type == 'I')
		{
			insert(value);
			counts(root);
			no_ops++;
			N++;
		}
		if (op_type == 'D')
		{
			deletion(value, root);
			counts(root);
			no_ops++;
			N--;
		}
	}
	cout<<"Set: "<<set<<" ("<<i<<")"<<endl;
	cout<<"Ops: "<<no_ops<<endl;
	cout<<"Mod: "<<modCount<<" "<<modCount/no_ops<<endl;
	cout<<"Tch: "<<touchCount<<" "<<touchCount/no_ops<<endl;
	cout<<"Max: "<< floor(log((no_ops/2+1)/2)/log(2));
	cout<<"\n\n";
	
	return 0;
}